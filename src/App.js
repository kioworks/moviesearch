import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import Index from './components/layout/Index';
import PostDetail from './components/movies/PostDetail';
import { Provider } from './context';
import { UserProvider } from './UserContext'
import Login from "./components/common/Login";


import './App.css';
import { UserConsumer } from './UserContext';


class App extends Component {
  render(){
    return (
      <Provider>
    <Router>
    <React.Fragment>
      <Navbar />
      <UserProvider>
      <Login  />
      </UserProvider>
      <div className="container">
        <Switch>
        <Route exact path="/" component={Index}></Route>
        <Route exact path="/movie/:id" component={PostDetail}></Route>
        </Switch>
      </div>
    </React.Fragment>
    </Router>
    </Provider>
  );
}};

export default App;
