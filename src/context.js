import React, { Component } from 'react';
import axios from 'axios';

const Context = React.createContext();

const reducer = (state, action) => {
    switch(action.type){
        case 'SEARCH_PET':
        return {
            ...state,
            pet_list: action.payload.results,
            heading: 'Search Results'
        };
        default:
            return state;
    }
}

export class Provider extends Component {
state={
    pet_list:[],
    heading: 'Latest Pets',
    dispatch: action => this.setState(state => reducer(state,action))
}

componentDidMount(){

    axios.get(`https://api.themoviedb.org/3/movie/top_rated?api_key=59c6b94a5f6c9f0e583fb9242558ad3f&language=en-US&page=1`)
    .then(res=>{
        //console.log(res.data.results)
        this.setState({pet_list:res.data.results})
    
    })
    .catch(err=>console.log(err))

}
    render() {
        return (
            <Context.Provider value={this.state}>
                {this.props.children}

            </Context.Provider>
        )
    }
}

export const Consumer = Context.Consumer;