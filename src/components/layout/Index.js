import React from 'react'
import Posts from '../movies/Posts';
import Search from '../movies/Search';

const Index =() => {
    return (
        <React.Fragment>
            <Search />
            <Posts />
        </React.Fragment>
    )
}
export default Index;
