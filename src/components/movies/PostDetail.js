import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class PostDetail extends Component {
    state = {
        post: {}
    }

    componentDidMount() {
        axios.get(`https://api.themoviedb.org/3/movie/${this.props.match.params.id}?api_key=59c6b94a5f6c9f0e583fb9242558ad3f&language=en-US`)

            .then(res => {
                console.log(res.data)
                this.setState({ post: res.data });

                return axios.get(`https://api.themoviedb.org/3/movie/${this.props.match.params.id}/credits?api_key=59c6b94a5f6c9f0e583fb9242558ad3f`)



            })
            .catch(err => console.log(err))
    }

    render() {
        const { post } = this.state;
        return (
            <React.Fragment>

                <Link to="/" className="btn btn-dark btn-sm mb-4">BACK</Link>
                <div className="card">
                    <h5 className="card-header">
                        {post.title} 
                    </h5>
                    <div className="card-body">
                        <p className="card-text">{post.info}</p>
        
                       
                    </div>

                </div>

            </React.Fragment>

        )
    }
}

export default PostDetail;
