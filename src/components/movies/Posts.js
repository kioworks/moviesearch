import React, { Component } from 'react'
import { Consumer } from '../../context'
import Spinner from '../layout/Spinner'
import Post from '../movies/Post'

class Posts extends Component {
    render() {
        return (
            <Consumer>
                {
                    value => {
                        console.log(value);

                        const { post_list, heading } = value;

                        if (post_list === undefined || post_list.length === 0) {
                            return <Spinner />

                        } else {
                            return (
                                <React.Fragment>
                        <h3 className="text-center mb-4">{heading}</h3>
                                    <div className="row">

                                        {post_list.map(item=>(
                                            <Post key={item.id} post={item}/>
                                        ))}

                                    </div>
                                </React.Fragment>

                            );

                        }
                    }
                }
            </Consumer>
        )
    }
}
export default Posts;