import React from 'react';
import { Link } from 'react-router-dom';


const Post = (props) => {
    const { post } = props;
    return (
        <div className="col-md-6">
            <div className="card mb-4 shadow-sm">
                <div className="card-body">
             <h4>{post.title}</h4>
             <p>{post.release_date}</p>
             <p className="card-text">{post.info}</p>
             <Link to={`/post/${post.id}`} className="btn btn-dark btn-block">
             <i className = "fas fa-chevron-right"></i> View Details
             </Link>
                </div>
            </div>
        </div>
    )
}
export default Post;