import React, { Component } from 'react'
import axios from 'axios';
import { Consumer } from '../../context'

class Search extends Component {

    state = {
        searchTerm: ''
    }

    onChange = (e)=>{ 

        this.setState({[e.target.name] : e.target.value});

    }

    findPost =(dispatch,e)=>{
        e.preventDefault();
        axios.get(`https://api.themoviedb.org/3/search/movie?api_key=59c6b94a5f6c9f0e583fb9242558ad3f&language=en-US&query=${this.state.searchTerm}&page=1&include_adult=false`)
    .then(res=>{
        dispatch ({
            type:'SEARCH_PET',
            payload:res.data
        })
        console.log(res.data);
    
    })
    .catch(err=>console.log(err))

    }



    render() {
        return (
            <Consumer>
                {value => {
                    const { dispatch } = value;
                    console.log ('dispatch!!!!!!'+dispatch);
                    return (
                        <div className="card card-body mb-4 p-4">
                            <h1 className="display-4 text-center">
                                <i className="fas fa-film"></i>Search
                            </h1>
                            <form onSubmit={this.findPost.bind(this, dispatch)}>
                                <input type="text"
                                    className="form-control form-control-lg"
                                    placeholder="ie. Location Breed etc..."
                                    name="postTitle"
                                    value={this.state.searchTerm}
                                    onChange={this.onChange}

                                />
                            </form>
                            <button className="btn btn-primary btn-lg btn-block mt-2 mb-5" type="submit">Find your pet</button>
                        </div>
                    )
                }

                }
            </Consumer>
        )
    }
}

export default Search;
