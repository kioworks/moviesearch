import React, { Component } from 'react';

export const UserContext = React.createContext();

export class UserProvider extends Component {
    state={
        user:{
            email: '',
            username:'',
            password:''
        }
        //dispatch: action => this.setState(state => reducer(state,action))
    }

    render() {
        return (
            <UserContext.Provider value={this.state}>
                {this.props.children}

            </UserContext.Provider>
        )
    }
}
    
export const UserConsumer = UserContext.Consumer;